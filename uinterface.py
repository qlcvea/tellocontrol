"""This module contains the UInterface class, which handles functionality for displaying images and overlaying information on them."""

import cv2, djitellopy, numpy as np, mediapipe as mp
from threading import Thread
from typing import List, Tuple

import draw_utils, xinput_handler

class UInterface:
    """Handles functionality for displaying images and overlaying information on them.
    
    Parameters
    ----------
    tello : djitellopy.Tello
        The Tello instance to display information from.
    xinh : xinput_handler.XInputHandler
        The XInputHandler instance to display information from.

    Attributes
    ----------
    faces
    mainFace
    frame
    title
    refreshMs
    """

    faces: List[Tuple[int, int, int, int]] = list()
    """List of faces as (x: int, y: int, width: int, height: int)."""

    hands = None
    """Hands."""

    mainFace: int = -1
    """Index of the main face."""

    frame = np.zeros((480, 640, 3), np.uint8)
    """The image to be displayed.
    
    Additional information (such as face boundaries, battery level, active controller index) will be drawn on top of a copy of this image.
    """

    title = 'camera'
    """Window title. Changes to this value wile the thread is running will have no effect."""

    refreshMs = int((1/60) * 1000)
    """Amount of time (in milliseconds) to wait between refreshes of the window content.
    
    This is not the actual amount of time between the start of one frame's rendering and the start of the next one, as rendering UI content takes a non-zero amount of time.
    """

    _doStop: bool = False
    """Private. If true, the thread is stopped or is stopping. (While loop exit condition)"""

    _tello: djitellopy.Tello = None
    """Private. The Tello instance that information is being displayed from."""

    _thread: Thread = None
    """Private. The thread that this class is wrapping."""

    _xinh: xinput_handler.XInputHandler = None
    """Private. The XInputHandler instance that information is being displayed from."""

    def __init__(self, tello: djitellopy.Tello, xinh: xinput_handler.XInputHandler):
        """Initializes the class.
        
        Parameters
        ----------
        tello : djitellopy.Tello
            The Tello instance to display information from.
        xinh : xinput_handler.XInputHandler
            The XInputHandler instance to display information from.
        """
        self._tello = tello
        self._xinh = xinh
        self._mpDrawing = mp.solutions.drawing_utils
        self._mpHands = mp.solutions.hands

    def start(self):
        """Starts the thread that will create the UI window and draw frames + overlays inside it.
        
        Raises
        ------
        Error
            If the thread is already running.
        """
        if not self.getStopped():
            raise Exception("Thread already running.")

        self._thread = Thread(target = self._threadRoutine, args = (), daemon = True)
        self._thread.start()

    def join(self):
        """Wait until the thread that this class is running terminates.
        
        Raises
        ------
        Error
            If the thread is not running.
        """
        if self.getStopped():
            raise Exception("Thread not running.")

        self._thread.join()

    def stop(self):
        """Signal to the thread that it should terminate.
        
        Raises
        ------
        Error
            If the thread is not running.
        """
        if self.getStopped():
            raise Exception("Thread not running.")

        self._doStop = True

    def getStopped(self) -> bool:
        """Returns true if the thread is not running, false otherwise.
        
        Returns
        -------
        bool
            True if the thread is not running, false otherwise.
        """
        return self._thread == None or not self._thread.is_alive()

    def _threadRoutine(self):
        """Private. The main function for the thread."""
        self._doStop = False

        # generate an initial frame containing the text "No image"
        self.frame = np.zeros((480, 640, 3), np.uint8)
        cv2.putText(self.frame, 
            'No image',
            (160, 240), 
            cv2.QT_FONT_NORMAL, 2, 
            (255, 255, 255), 
            2, 
            cv2.LINE_4)

        title = str(self.title) # ensure that the value of title does not change while the thread is running
        cv2.namedWindow(title, cv2.WINDOW_AUTOSIZE)

        while not self._doStop:
            if not cv2.getWindowProperty(title, cv2.WND_PROP_VISIBLE):
                break
            uiFrame = self.frame.copy()

            # draw faces
            i = 0
            for face in self.faces:
                color = (0, 0, 255)
                if (i == self.mainFace):
                    color = (0, 255, 0) # main face uses a different color for drawn items

                (x, y, w, h) = face

                cv2.rectangle(uiFrame, (x, y), (x + w, y + h), color, 2)
                cv2.circle(uiFrame, (int(x + (w / 2)), int(y + (h / 2))), 1, color, 2)
                cv2.putText(self.frame, 
                    str(w * h),
                    (x + 5, y + h - 5), 
                    cv2.QT_FONT_NORMAL, 0.5, 
                    color, 
                    2, 
                    cv2.LINE_4)
                i += 1

            # draw hands
            if self.hands != None and self.hands.multi_hand_landmarks:
                for hand_landmarks in self.hands.multi_hand_landmarks:
                    self._mpDrawing.draw_landmarks(uiFrame, hand_landmarks, self._mpHands.HAND_CONNECTIONS)

            # draw status information
            frameStr = 'B:' + str(self._tello.get_battery()) + '%'
            
            if self._xinh.userIndex >= 0:
                frameStr = frameStr + ' C:' + str(self._xinh.userIndex)

            draw_utils.drawTextBg(uiFrame, 
                frameStr,
                (3, 5),
                cv2.QT_FONT_NORMAL, 1, 1,
                (255, 255, 255), 
                (0, 0, 0, 0))

            if len(self._xinh.menu) > 0:
                # draw menu from XInputHandler
                i = 0
                y = 50
                for menuItem in self._xinh.menu:
                    color = (255, 255, 255)
                    if i == self._xinh.menuPos:
                        if self._xinh.menuProcessing:
                            color = (0, 255, 0) # different color for active item when processing (green)
                        else:
                            color = (255, 255, 0) # different color for active item (cyan)

                    y += draw_utils.drawTextBg(uiFrame,
                        menuItem[0],
                        (3, y),
                        cv2.QT_FONT_NORMAL, 1, 1,
                        color, 
                        (0, 0, 0, 0))[1] + 1 # draw_utils.drawTextBg returns (text_w, text_h); +1 just to be safe
                    i += 1

            # finished, show image and wait
            cv2.imshow(self.title, uiFrame)
            cv2.waitKey(self.refreshMs)

        if cv2.getWindowProperty(title, cv2.WND_PROP_VISIBLE):
            cv2.destroyWindow(title)