"""This module contains the FaceProcessor class, which handles functionality for finding faces and determining the main face."""

import cv2
from typing import List, Tuple

class FaceProcessor:
    """Handles functionality for finding faces and determining the main face."""

    _cascade: cv2.CascadeClassifier = None
    """Private. The CV2 cascade that is being used to find faces in an image."""

    def __init__(self):
        """Initializes the class."""
        self._cascade = cv2.CascadeClassifier('./cascades/haarcascade_frontalface_default.xml')

    def detect(self, frame, previousFaces: List[Tuple[int, int, int, int]] or None = None, previousMain: int or None = None) -> Tuple[List[Tuple[int, int, int, int]], int]:
        """Detects faces in an image and determines what the main face is.

        Parameters
        ----------
        frame
            The frame to find faces inside of.
        previousFaces: List[Tuple[int, int, int, int]] or None, optional
            The faces returned by this function when it was run on the frame that preceded the one being processed, as (x: int, y: int, width: int, height: int).
            If there is no previous frame, None.
            This parameter is currently unused, but it may be used in the future to better determine the main face.
        previousMain: List[Tuple[int, int, int, int]] or None, optional
            The value of "main" (index of main face) returned by this function when it was run on the frame that preceded the one being processed.
            If there is no previous frame, None.
            This parameter is currently unused, but it may be used in the future to better determine the main face.
        
        Returns
        -------
        faces: List[Tuple[int, int, int, int]]
            List of identified faces as (x: int, y: int, width: int, height: int)
        main : int
            The index of the main face. If < 0, there is no main face.
        """
        grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = self._cascade.detectMultiScale(grayFrame, 1.2, 8)

        # identify the main face
        main = -1
        if len(faces) > 0:
            mainArea = 0
            i = 0
            for face in faces:
                (x, y, w, h) = face
                area = w * h
                if mainArea < area:
                    mainArea = area
                    main = i
                i += 1

        return faces, main

    def getMainFace(self, faces: List[Tuple[int, int, int, int]], main: int) -> Tuple[int, int, int, int] or None:
        """Get the main face from a list of faces returned by FaceProcessor.detect()

        Parameters
        ----------
        faces: List[Tuple[int, int, int, int]]
            List of faces as (x: int, y: int, width: int, height: int)
        main : int
            The index of the main face. (< 0 = no main face).

        Returns
        -------
        Tuple[int, int, int, int] or None
            The main face as (x: int, y: int, w: int, h: int) if it was found, or None if it was not.
        """
        if main < 0 or main >= len(faces):
            return None
        else:
            return faces[main]

    def createControls(self, faces: List[Tuple[int, int, int, int]], main: int, frameShape: Tuple[int, int]) -> Tuple[int, int, int, int] or None:
        """Create RC values from faces

        Parameters
        ----------
        faces: List[Tuple[int, int, int, int]]
            List of faces as (x: int, y: int, width: int, height: int)
        main : int
            The index of the main face. (< 0 = no main face).
        frameShape : int
            The shape of the frame as (height : int, width : int).

        Returns
        -------
        left_right_velocity : int
        forward_backward_velocity : int
        up_down_velocity : int
        yaw_velocity : int
        """
        left_right_velocity = 0
        forward_backward_velocity = 0
        up_down_velocity = 0
        yaw_velocity = 0

        frameHeight = frameShape[0]
        frameWidth = frameShape[1]
        # using this instead of (frameHeight, frameWidth) = frameShape because the frame array shape contains a third value (= 3) to indicate RGB
        mainFace = self.getMainFace(faces, main)

        if not mainFace is None:
            (x, y, w, h) = mainFace
            xCenter = int(x + (w / 2))
            yCenter = int(y + (h / 2))

            # compute forward_backward_velocity based on area
            area = w * h
            if area > 40000:
                forward_backward_velocity = -20
            elif area < 30000:
                forward_backward_velocity = 20

        # ensure values are within bounds
        if left_right_velocity > 100: left_right_velocity = 100
        if left_right_velocity < -100: left_right_velocity = -100
        if forward_backward_velocity > 100: forward_backward_velocity = 100
        if forward_backward_velocity < -100: forward_backward_velocity = -100
        if up_down_velocity > 100: up_down_velocity = 100
        if up_down_velocity < -100: up_down_velocity = -100
        if yaw_velocity > 100: yaw_velocity = 100
        if yaw_velocity < -100: yaw_velocity = -100

        return left_right_velocity, forward_backward_velocity, up_down_velocity, yaw_velocity