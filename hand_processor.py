"""This module contains the HandProcessor class, which handles functionality for controlling a Tello based on hand gestures."""

import cv2, numpy as np, mediapipe as mp
from typing import Tuple

MAX_X_VELOCITY = 20
MAX_Y_VELOCITY = 30

class HandProcessor:
    """Handles functionality for controlling a Tello based on hand gestures."""

    def __init__(self):
        """Initializes the class."""
        self._mpHands = mp.solutions.hands
        self._hands = self._mpHands.Hands()

    def detect(self, frame):
        """Detects hands in an image.

        Parameters
        ----------
        frame
            The frame to find hands inside of.
        
        Returns
        -------
        hands
        """
        rgbframe = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        return self._hands.process(rgbframe)

    
    def createControls(self, hands) -> Tuple[int, int, int, int]:
        """Create RC values from hands

        Parameters
        ----------
        hands

        Returns
        -------
        left_right_velocity : int
        forward_backward_velocity : int
        up_down_velocity : int
        yaw_velocity : int
        """
        left_right_velocity = 0
        forward_backward_velocity = 0
        up_down_velocity = 0
        yaw_velocity = 0

        if hands.multi_hand_landmarks:
            for hand_landmarks in hands.multi_hand_landmarks:
                # landmarks are landmark[index].x (or y)
                landmarks = hand_landmarks.landmark

                # find center of hand from wrist (0), index_mcp (5), pinky_mcp (17) as [x, y]
                center = [ (landmarks[0].x + landmarks[5].x + landmarks[17].x) / 3, (landmarks[0].y + landmarks[5].y + landmarks[17].y) / 3 ]
                
                thumb_tip = landmarks[4]

                # find distance from wrist to thumb_tip (radius of circle, which is hypothenuse of triangle with sides defined by motion vector),
                radius = np.sqrt(np.square(center[0] - thumb_tip.x) + np.square(center[1] - thumb_tip.y))
                # then take 50% of that as max distance from wrist to other fingers
                max_distance = radius * 0.5
                
                # check if all fingers except for thumb are close to wrist
                # tips are at indexes 8, 12, 16, 20 (thumb is 4)
                allow_motion = True
                for i in range(8, 21, 4):
                    tip = landmarks[i]
                    distance = np.sqrt(np.square(tip.x - center[0]) + np.square(tip.y - center[1]))
                    if (distance > max_distance):
                        allow_motion = False
                        break
                
                if allow_motion:
                    # create motion vector from hand center to thumb_tip as [x, y]
                    motion = [ center[0] - thumb_tip.x, center[1] - thumb_tip.y ]

                    # r : vmax = l : v
                    # v = (vmax * l) / r
                    # vmax = (max velocity for vector component that is being calculated)
                    # l = tx - cx || ty - cy (motion vector x or y)
                    # v = vx || vy (x or y velocity)
                    left_right_velocity = -int((MAX_X_VELOCITY * motion[0]) / radius)
                    up_down_velocity = int((MAX_Y_VELOCITY * motion[1]) / radius)

        return left_right_velocity, forward_backward_velocity, up_down_velocity, yaw_velocity