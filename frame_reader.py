"""This module contains the FrameReader class, which handles functionality for reading frames from a Tello and processing them."""

import asyncio
import djitellopy, numpy as np
from threading import Thread
from typing import List, Tuple
from hand_processor import HandProcessor

import uinterface
from face_processor import FaceProcessor

class FrameReader:
    """Handles functionality for reading frames from a Tello and processing them as needed.
    
    Parameters
    ----------
    tello : djitellopy.Tello
        The Tello instance to read frames from.
    ui : uinterface.UInterface or None, optional
        A UInterface to push frames to for viewing.

    Attributes
    ----------
    faces
    mainFace
    frame
    controls
    """

    faces: List[Tuple[int, int, int, int]] = None
    """Read-only. List of faces as (x: int, y: int, width: int, height: int)."""

    hands = None
    """Read-only. Hands"""

    mainFace: int = None
    """Read-only. Index of the main face."""

    frame = np.zeros((480, 640, 3), np.uint8)
    """Read-only. The most recent frame from the Tello."""

    faceControls: Tuple[int, int, int, int] = (0, 0, 0, 0)
    """Read-only. The RC controls that were created by processing the faces in the current frame as (left_right_velocity, forward_backward_velocity, up_down_velocity, yaw_velocity)."""

    handControls: Tuple[int, int, int, int] = (0, 0, 0, 0)
    """Read-only. The RC controls that were created by processing the hands in the current frame as (left_right_velocity, forward_backward_velocity, up_down_velocity, yaw_velocity)."""

    _doStop: bool = False
    """Private. If true, the thread is stopped or is stopping. (While loop exit condition)"""

    _facer: FaceProcessor = FaceProcessor()
    """Private. The instance of FaceProcessor that is being used to find faces and create RC values."""

    _hander: HandProcessor = HandProcessor()
    """Private. The instance of HandProcessor that is being used to find hands and create RC values."""

    _tello: djitellopy.Tello = None
    """Private. The Tello instance that frames are being read from."""

    _thread: Thread = None
    """Private. The thread that this class is wrapping."""

    _ui: uinterface.UInterface or None = None
    """Private. The UInterface that frames are being pushed to."""

    def __init__(self, tello: djitellopy.Tello, ui: uinterface.UInterface or None = None):
        """Initializes the class.
        
        Parameters
        ----------
        tello : djitellopy.Tello
            The Tello instance to read frames from.
        ui : uinterface.UInterface or None, optional
            A UInterface to push frames to for viewing.
        """
        self._tello = tello
        self._ui = ui

    def start(self):
        """Starts the thread.
        
        Raises
        ------
        Error
            If the thread is already running.
        """
        if not self.getStopped():
            raise Exception("Thread already running.")

        self._thread = Thread(target = self._threadRoutine, args = (), daemon = True)
        self._thread.start()

    def join(self):
        """Wait until the thread that this class is running terminates.
        
        Raises
        ------
        Error
            If the thread is not running.
        """
        if self.getStopped():
            raise Exception("Thread not running.")

        self._thread.join()

    def stop(self):
        """Signal to the thread that it should terminate.
        
        Raises
        ------
        Error
            If the thread is not running.
        """
        if self.getStopped():
            raise Exception("Thread not running.")

        self._doStop = True

    def getStopped(self) -> bool:
        """Returns true if the thread is not running, false otherwise.
        
        Returns
        -------
        bool
            True if the thread is not running, false otherwise.
        """
        return self._thread == None or not self._thread.is_alive()

    async def _updateFaces(self):
        """Private. Update values of self.faces, self.mainFace (+ equivalents in self._ui), and self.faceControls based on self.frame."""
        self.faces, self.mainFace = self._facer.detect(self.frame, self.faces, self.mainFace)
        if self._ui != None:
            self._ui.frame = self.frame
            self._ui.faces = self.faces
            self._ui.mainFace = self.mainFace
        self.faceControls = self._facer.createControls(self.faces, self.mainFace, self.frame.shape)

    async def _updateHands(self):
        """Private. Update values of self.hands (+ equivalent in self._ui), and self.handControls based on self.frame."""
        self.hands = self._hander.detect(self.frame)
        if self._ui != None:
            self._ui.hands = self.hands
        self.handControls = self._hander.createControls(self.hands)

    async def _asyncThreadRoutine(self):
        """Private. The main function for the thread."""
        self._doStop = False

        while not self._doStop:
            self.frame = self._tello.get_frame_read().frame
            await asyncio.gather(self._updateFaces(), self._updateHands())

    def _threadRoutine(self):
        """Private. Starts the main function for the thread using asyncio.run."""
        asyncio.run(self._asyncThreadRoutine())