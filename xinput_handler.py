"""This module contains the XInputHandler class, which handles all XInput related functionality."""

import djitellopy, time, XInput as xin
from threading import Thread
from typing import List, Tuple, Callable

class XInputHandler:
    """Handles all XInput related functionality.
    
    This class handles all of XInput, not just a single controller.
    It takes care of choosing which controller's inputs should be considered and processing those inputs.

    Parameters
    ----------
    djitellopy : djitellopy.Tello
        The Tello instance to be controlled.

    Attributes
    ----------
    defaultMenu
    controls
    menu
    menuPos
    menuProcessing
    userIndex
    """

    menu: List[Tuple[str, Callable]] = list()
    """Read-only. Active menu. If None there is no active menu."""

    menuPos: int = 0
    """Read-only. Currently selected menu item."""

    menuProcessing: bool = False
    """Read-only. If true, an action that was selected in the menu is being performed."""

    defaultMenu: List[Tuple[str, Callable]] = list()
    """Default menu.
    
    This menu will be opened when the action button (Menu (One/Series) / Start (360)) is pressed, with an additional "Relinquish control" item at the beginning.
    """

    controls: Tuple[int, int, int, int] = (0, 0, 0, 0)
    """Read-only. The RC controls that are being inputted from the currently active controller as (left_right_velocity, forward_backward_velocity, up_down_velocity, yaw_velocity).
    
    If there is no active controller, the value will be (0, 0, 0, 0)
    """

    refreshSec = 0.1
    """Amount of time (in seconds) to wait between attempting to read controller values.
    
    This is not the actual amount of time between the start of one read and the start of the next one, as processing the values takes a non-zero amount of time.
    """

    userIndex: int = -1
    """Read-only. Index of the currently active controller. If < 0, no controller is active."""

    _tello: djitellopy.Tello = None
    """Private. The Tello instance that is being controlled."""

    _thread: Thread = None
    """Private. The thread that this class is wrapping."""

    def __init__(self, tello: djitellopy.Tello):
        """Initializes the class.
        
        Parameters
        ----------
        tello : djitellopy.Tello
            The Tello instance to be controlled.
        """
        self._tello = tello

    def start(self):
        """Starts the thread that will check for controller inputs and control the Tello accordingly.
        
        Raises
        ------
        Error
            If the thread is already running.
        """
        if not self.getStopped():
            raise Exception("Thread already running.")

        self._thread = Thread(target = self._threadRoutine, args = (), daemon = True)
        self._thread.start()

    def join(self):
        """Wait until the thread that this class is running terminates.
        
        Raises
        ------
        Error
            If the thread is not running.
        """
        if self.getStopped():
            raise Exception("Thread not running.")

        self._thread.join()

    def stop(self):
        """Signal to the thread that it should terminate.
        
        Raises
        ------
        Error
            If the thread is not running.
        """
        if self.getStopped():
            raise Exception("Thread not running.")

        self.userIndex = -2

    def getStopped(self) -> bool:
        """Returns true if the thread is not running, false otherwise.
        
        Returns
        -------
        bool
            True if the thread is not running, false otherwise.
        """
        return self._thread == None or not self._thread.is_alive()

    def _relinquishControl(self):
        """Private. Relinquish control.
        
        Used to create the first item in the default menu.
        """
        self.userIndex = -1
        self.controls = (0, 0, 0, 0)

    def _menuExecThread(self, menuAction: Callable):
        """Private. Execute menu item (wrapped to change the value of self.menuProcessing)."""
        self.menuProcessing = True
        menuAction()
        self.menu = list()
        self.menuProcessing = False

    def _threadRoutine(self):
        """Private. The main function for the thread."""
        self.userIndex = -1

        while self.userIndex != -2:
            events = xin.get_events()
            for event in events:
                # check for emergency button (View (One / Series) / Back (360)) independently from anything else
                # the button may be pressed on any controller, not just the currently active one (if there is one)
                if event.type == xin.EVENT_BUTTON_PRESSED:
                    if event.button_id == xin.BUTTON_BACK:
                        self._tello.emergency()
                        break
                
                # check for activity from the currently active controller
                if event.user_index == self.userIndex:
                    # relinquish control if disconnected
                    if event.type == xin.EVENT_DISCONNECTED:
                        self._relinquishControl()
                    elif event.type == xin.EVENT_BUTTON_PRESSED:
                        if len(self.menu) > 0:
                            # in a menu
                            if not self.menuProcessing:
                                # only consider events if nothing is being processed already
                                if event.button_id == xin.BUTTON_START or event.button_id == xin.BUTTON_B:
                                    # exit menu by pressing action button (Menu (One / Series) / Start (360)) or B
                                    self.menu = list()
                                elif event.button_id == xin.BUTTON_A:
                                    # press A to execute currently selected item and close menu
                                    #_mt = Thread(target = self._menuExecThread, args = (self.menu[self.menuPos][1]), daemon = True)
                                    _mt = Thread(target = self._menuExecThread, args = (self.menu[self.menuPos][1], ), daemon = True)
                                    _mt.start()
                                elif event.button_id == xin.BUTTON_DPAD_UP:
                                    # move up
                                    newIndex = self.menuPos - 1
                                    if newIndex < 0: newIndex = len(self.menu) - 1 # wrap
                                    self.menuPos = newIndex
                                elif event.button_id == xin.BUTTON_DPAD_DOWN:
                                    # move down
                                    newIndex = self.menuPos + 1
                                    if newIndex >= len(self.menu): newIndex = 0 # wrap
                                    self.menuPos = newIndex
                        else:
                            # not in a menu
                            if event.button_id == xin.BUTTON_START:
                                # open default menu (+ Relinquish control) by pressing action button (Menu (One / Series) / Start (360))
                                self.menuPos = 0
                                self.menuProcessing = False
                                newMenu = list()
                                newMenu.append(('Relinquish control', self._relinquishControl))
                                newMenu.extend(self.defaultMenu)
                                self.menu = newMenu
                            elif event.button_id == xin.BUTTON_DPAD_UP:
                                self._tello.flip_forward()
                            elif event.button_id == xin.BUTTON_DPAD_DOWN:
                                self._tello.flip_back()
                            elif event.button_id == xin.BUTTON_DPAD_LEFT:
                                self._tello.flip_left()
                            elif event.button_id == xin.BUTTON_DPAD_RIGHT:
                                self._tello.flip_right()
                
                # check for activity from controllers that are not the currently active one
                else:
                    if event.type == xin.EVENT_BUTTON_PRESSED:
                        # press action button (Menu (One / Series) / Start (360)) to claim control
                        if event.button_id == xin.BUTTON_START:
                            self.userIndex = event.user_index
            
            # read sticks + triggers from currently active controller if there is one
            if self.userIndex >= 0 and self.userIndex < 4:
                state = xin.get_state(self.userIndex)

                (lx, ly), (rx, ry) = xin.get_thumb_values(state)
                (lt, rt) = xin.get_trigger_values(state)

                self.controls = (int(lx * 100), int(ly * 100), int(rt * 100 - lt * 100), int(rx * 100))

        self._relinquishControl()