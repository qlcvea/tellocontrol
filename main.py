import cv2, time
from datetime import datetime
from djitellopy import Tello

from face_processor import FaceProcessor
from frame_reader import FrameReader
from uinterface import UInterface
from xinput_handler import XInputHandler

def main():
    tello = Tello()
    tello.connect()

    xinh = XInputHandler(tello)
    def _menuLandCb(): tello.land()
    xinh.defaultMenu.append(('Land', _menuLandCb))
    def _menuTakeoffCb(): tello.takeoff()
    xinh.defaultMenu.append(('Takeoff', _menuTakeoffCb))
    def _menuFaceCb(): xinh.userIndex = 5
    xinh.defaultMenu.append(('Begin face tracking', _menuFaceCb))
    def _menuHandCb(): xinh.userIndex = 6
    xinh.defaultMenu.append(('Begin hand tracking', _menuHandCb))
    def _menuPhotoCb(): cv2.imwrite(datetime.now().strftime('%Y-%m-%d_%H%M%S') + '.png', ui.frame)
    xinh.defaultMenu.append(('Take photo', _menuPhotoCb))
    def _menuEndCb(): xinh.stop()
    xinh.defaultMenu.append(('Terminate', _menuEndCb))
    xinh.start()

    ui = UInterface(tello, xinh)
    ui.start()

    tello.streamon()

    frameReader = FrameReader(tello, ui)
    frameReader.start()

    previousUI = None
    previousControls = None

    try:
        while True:
            if xinh.getStopped() or ui.getStopped():
                break
            if previousUI != xinh.userIndex:
                print('changed user index: ' + str(xinh.userIndex))
                previousUI = xinh.userIndex

            if xinh.userIndex == 5:
                # controlled by faces
                controls = frameReader.faceControls
            elif xinh.userIndex == 6:
                # controlled by hand
                controls = frameReader.handControls
            else:
                controls = xinh.controls

            if controls != previousControls:
                tello.send_rc_control(*controls)
                previousControls = controls

            time.sleep(tello.TIME_BTW_RC_CONTROL_COMMANDS)
    except KeyboardInterrupt:
        pass

    if not xinh.getStopped(): xinh.stop()
    if not ui.getStopped(): ui.stop()
    tello.streamoff()

    print('finished')

if __name__ == '__main__':
    main()